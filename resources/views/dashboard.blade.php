@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">


        <form action="/dashboard" >
            <input type="date" name="date">
            <input type="button" value="Submit">
        </form>

        <br>
        <br>

        <div class="by-date">
            <h4>By Date</h4>
            <table class="table table-stripped">
                <tr>
                    <th>id_sales_order_item</th>
                    <th>Name</th>
                    <th>SKU</th>
                </tr>
                
            <table>
        </div>


        <div class="question-1">
            <h4>Question 1</h4>
            <table class="table table-stripped">
                <tr>
                    <th>id_sales_order_item</th>
                    <th>SKU</th>
                    <th>Name</th>
                    <th>Unit price</th>
                </tr>
                  <tr>
                    <td>{{$orderItem->id_sales_order_item}}</td>
                    <td>{{$orderItem->sku}}</td>
                    <td>{{$orderItem->name}}</td>
                    <td>{{$orderItem->unit_price}}</td>
                  </tr>
                  
            <table>
        </div>

        <br>
        
        <div class="question-2">
            <h4>Question 2</h4>
            <table class="table table-stripped">
                <tr>
                    <th>id_sales_order_item</th>
                    <th>SKU</th>
                    <th>Name</th>
                    <th>Unit price</th>
                </tr>
                @foreach ($itemDeliveredAtWarehouse as $value)
                  <tr>
                    <td>{{$value->id_sales_order_item}}</td>
                    <td>{{$value->sku}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->status->status}}</td>
                  </tr>
                  @endforeach
            <table>
        </div>
        
        <br>

        <div class="question-3">
            <h4>Question 3</h4>
            <table class="table table-stripped">
                <tr>
                    <th>Total Unit Price</th>
                </tr>
                    <td>{{$totalUnitPrice}}</td>
            <table>
        </div>

        <br>

        <div class="question-6">
            <h4>Question 6</h4>
            <table class="table table-stripped">
                <tr>
                    <th>fk_sales_order</th>
                    <th>id_sales_order_item</th>
                    <th>Name</th>
                    <th>SKU</th>
                    <th>Status</th>
                </tr>

                @foreach ($itemsWithLastTwoStatus as $value)
                  <tr>
                    <td>{{$value['fk_sales_order']}}</td>
                    <td>{{$value['id_sales_order_item']}}</td>
                    <td>{{$value['name']}}</td>
                    <td>{{$value['sku']}}</td>
                    <td>{{$value['last_status']}}</td>
                  </tr>
                @endforeach
            <table>
        </div>

        <hr>

        
    </div>
</div>
@endsection
