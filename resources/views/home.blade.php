
@extends('layouts.app')

@section('content')
    <div class="row text-center">
        <div class="package-volume chart">
            <h2>Package Volume</h2>
             <canvas id="package_volume" width="600" height="400"></canvas>
        </div>

        <div class="leadtime-average chart">
            <h2>Lead Time Average</h2>
             <canvas id="leadtime_average" width="600" height="400"></canvas>
        </div>
    </div>
@endsection
