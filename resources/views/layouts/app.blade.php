<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href=https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css rel="stylesheet">
    <link href="{{ mix('/css/app.css')}}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>

    <div class="text-center">
        <h2><a href="/">Graph</a> | <a href="/dashboard">Dashboard</a></h2>
    </div>
    <br>
    @yield('content')

    <!-- Scripts -->
    <script src="{{asset('/js/app.js')}}" defer></script>

    <script src="{{asset('/js/Chart.min.js')}}" defer></script>

    <script src="{{asset('/js/custom.js')}}" defer></script>
</body>
</html>