
var charts = {
	init: function () {
		this.ajaxGetPackageVolume();
		this.ajaxGetLeadtimeAverage();

		// this.createCompletedJobsChart('test');
	},

	ajaxGetPackageVolume: function () {
		var path =  'http://' + window.location.hostname + ':8000/get/package/volume';
		var request = $.ajax( {
			method: 'GET',
			url: path
		}).done( function ( response ) {
			charts.createPackageVolumeChart( response );
		});
	},

	ajaxGetLeadtimeAverage: function () {
		var path =  'http://' + window.location.hostname + ':8000/get/average/leadtime';
		var request = $.ajax( {
			method: 'GET',
			url: path
		}).done( function ( response ) {
			charts.createLeadtimeAverageChart( response );
		});
	},

	/**
	 * Created the Completed Jobs Chart
	 */
	createPackageVolumeChart: function ( response ) {
		var ctx = document.getElementById("package_volume").getContext('2d');
	    var myChart = new Chart(ctx, {
	        type: 'doughnut',
	        data: {
	            labels: response.major_region,
	            datasets: [{
	                label: 'Package Volume',
	                data: response.total,
	                backgroundColor: [
	                    'rgba(255, 99, 132, 1)',
	                    'rgba(54, 162, 235, 1)',
	                    'rgba(255, 206, 86, 1)',
	                    'rgba(255, 159, 64, 1)'
	                ],
	                borderColor: [
	                    'rgba(255,99,132,1)',
	                    'rgba(54, 162, 235, 1)',
	                    'rgba(255, 206, 86, 1)',
	                    'rgba(255, 159, 64, 1)'
	                ],
	                borderWidth: 1
	            }]
	        },
	       
	    });
	},
	createLeadtimeAverageChart: function ( response ) {

		var ctx = document.getElementById("leadtime_average").getContext('2d');
	    var myChart = new Chart(ctx, {
	        type: 'bar',
	        data: {
	            labels: response.major_region,
	            datasets: [{
	                label: 'Lead Time Average',
	                data: response.leadtime_average,
	                backgroundColor: [
	                    'rgba(255, 99, 132, 1)',
	                    'rgba(54, 162, 235, 1)',
	                    'rgba(255, 206, 86, 1)',
	                    'rgba(255, 159, 64, 1)'
	                ],
	                borderColor: [
	                    'rgba(255,99,132,1)',
	                    'rgba(54, 162, 235, 1)',
	                    'rgba(255, 206, 86, 1)',
	                    'rgba(255, 159, 64, 1)'
	                ],
	                borderWidth: 1
	            }]
	        },
	        options: {
	            scales: {
	                yAxes: [{
	                    position: "left",
	                    scaleLabel: {
	                        display: true,
	                        labelString: "Days",
	                        fontColor: "black",
	                    },
	                    ticks: {
	                        min: 0,
	                        max: 6,
	                    },
	                }]
	            }
	        }
	    });
	}
};

charts.init();