<?php

namespace App\Http\Controllers;

use App\Item;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function dashboard() 
    {	
    	$data = [];
    	$orderItem = Item::where('id_sales_order_item', '229884')->first();

    	// get item delivered
    	$itemDeliveredAtWarehouse = Item::with('status')
    		->whereHas('status', function($query) {
		    	$query->where('status', 'delivered');
			})
    		->where('shipping_type', 'warehouse')
    		->get();

    	// get total unit price
    	$totalUnitPrice = Item::where('shipping_type', 'cross_docking')->sum('unit_price');

    	$historyStatus = Item::with('history.status')->get()->toArray();

		$itemsWithLastTwoStatus=[];
		// loop history
    	foreach ($historyStatus as $key => $value) {
    		$itemsWithLastTwoStatus[$key] = $value;
    		
			$c = 1;
    		foreach ($value['history'] as $val) {
    			if (count($value['history']) > 2) {
	    			if ($c == 2) {
	    				//get only last two status
		    			$itemsWithLastTwoStatus[$key]['last_status'] = $val['status']['status'];
	    			}

	    			$c++;
    			} else {
    				$itemsWithLastTwoStatus[$key]['last_status'] = $val['status']['status'];
    			}
    		}
    	}

    	return view('dashboard', [
            'orderItem' => $orderItem, 
            'totalUnitPrice' => $totalUnitPrice, 
            'itemDeliveredAtWarehouse' => $itemDeliveredAtWarehouse, 
            'itemsWithLastTwoStatus' => $itemsWithLastTwoStatus, 
        ]);
    }
}
