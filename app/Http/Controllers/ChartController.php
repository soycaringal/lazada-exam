<?php

namespace App\Http\Controllers;

use App\Package;
use DB;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function getPackageVolume() 
    {
    	$data = [];
    	$packages = DB::table('packages')
    		->select('major_region', 
    			DB::raw('count(*) as total')
    		)
    		->groupBy('major_region')
    		->get();

    	foreach ($packages as $key => $package) {
			$data['major_region'][] = $package->major_region;
			$data['total'][] = $package->total;
    	}

    	return $data;
    }

    public function getAverageLeadTime() 
    {
    	$data = [];
    	$packages = DB::table('packages')
    		->select(
    			'major_region', 
    			DB::raw('count(*) as total'),
    			DB::raw('sum(leadtime) as total_leadtime')
    		)
    		->groupBy('major_region')
    		->get();

    	foreach ($packages as $key => $package) {
			$data['major_region'][] = $package->major_region;
			$data['leadtime_average'][] = number_format(( $package->total_leadtime / $package->total), 2);
    	}

    	return $data;
    }
}
