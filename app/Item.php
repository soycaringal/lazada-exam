<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	public $table = 'ims_sales_order_item';

	public function status()
    {
        return $this->hasOne('App\Status', 'id_sales_order_item_status', 'fk_sales_order_item_status');
    }

    public function history()
    {
        return $this->hasMany('App\History', 'fk_sales_order_item', 'id_sales_order_item')->orderBy('created_at', 'DESC');
    }
}
