<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    	public $table = 'ims_sales_order_item_status_history';

    public function status()
    {
        return $this->hasOne('App\Status', 'id_sales_order_item_status', 'fk_sales_order_item_status');
    }
}
